﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using NotesFor.HtmlToOpenXml;

namespace openXMLTest
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            const string filename = "test.doc";
            if (File.Exists(filename)) File.Delete(filename);
             
            var doc =
                XDocument.Load(
                    @"c:\users\inferon\documents\visual studio 2013\Projects\openXMLTest\openXMLTest\Page.html");

            using (var generatedDoc = new MemoryStream())
            {
                using (var package = WordprocessingDocument.Create(generatedDoc, WordprocessingDocumentType.Document))
                {
                    var mainPart = package.MainDocumentPart;
                    if (mainPart == null)
                    {
                        mainPart = package.AddMainDocumentPart();
                        new Document(new Body(
                            new SectionProperties(
                                new PageMargin()
                                {
                                    Top = 0,
                                    Right = (UInt32Value)500UL,
                                    Bottom = 0,
                                    Left = (UInt32Value)500UL,
                                    Header = (UInt32Value)0UL,
                                    Footer = (UInt32Value)0UL,
                                    Gutter = (UInt32Value)0UL
                                } 
                                )))
                        .Save(mainPart);
                    }

                    var converter = new HtmlConverter(mainPart)
                    {
                        ImageProcessing = ImageProcessing.AutomaticDownload,
                        
                    };
                    var body = mainPart.Document.Body;
                    var paragraph = converter.Parse(doc.ToString());
                    foreach (var p in paragraph)
                        body.Append(p);

                    mainPart.Document.Save();
                }
                generatedDoc.Seek(0L, SeekOrigin.Begin);
                File.WriteAllBytes(filename, generatedDoc.ToArray());
            }
        }
    }
}
